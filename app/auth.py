from functools import wraps
from flask import redirect, session, abort

from app.models.roles.utilisateur import Utilisateur


class Auth:
    @staticmethod
    def connexion(login, password):
        users_found = Utilisateur.query.filter_by(login=login, password=password).all()

        if len(users_found)==1:
            user = users_found[0]
        else:
            user = None
        return user

    @staticmethod
    def user_connected(func):
        @wraps(func)
        def wrapper():
            user = session.get('user')
            if user:
                return func()
            else:
                return redirect('/connexion')
        return wrapper

    @staticmethod
    def role_in(*authorized_roles):
        def wrapper(func):
            @wraps(func)
            def wrapped():
                user = session.get('user')
                if user:
                    if user.get('role') in authorized_roles:
                        return func()
                    else:
                        return abort(403)
                else:
                    return redirect('/connexion')
            return wrapped
        return wrapper
