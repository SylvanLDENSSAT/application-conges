from app.server import server
from app.ctrl.utils import Auth, render, URL, redirect, notify_error
from app.models import Employe
from app.models.conge import STATUT_CONGE

@Auth.role_in('rrh')
@server.route(URL.PAGE.FICHE_EMPLOYE)
def fiche_employe(id_employe):
    employe = Employe.query.filter_by(id=id_employe).first()
    if employe:
        return render(
            'fiche_employe.html',
            employe=employe,
            STATUT_CONGE=STATUT_CONGE
        )
    else:
        notify_error("Aucun employé n'existe avec l'id %s"%id_employe)
        return redirect(URL.PAGE.GESTION_EMPLOYES)
