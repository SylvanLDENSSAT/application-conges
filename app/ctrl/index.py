from app.server import server
from app.ctrl.utils import session, redirect, URL

@server.route('/')
def ctrl_index():
    if not session.get('user'):
        return redirect(URL.PAGE.CONNEXION)
    else:
        return redirect(URL.PAGE.HOME)
