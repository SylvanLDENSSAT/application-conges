from sqlalchemy.exc import IntegrityError

from app.server import server
from app.ctrl.utils import Auth, render, request, redirect, abort, notify_error, notify_success, get_current_user
from app.ctrl.urls import URL

from app.models.roles.rrh import NomEquipeExistant

from app.models import Equipe

@server.route(URL.PAGE.GESTION_EQUIPES)
@Auth.role_in('rrh')
def ctrl_gestion_equipes():
    equipes = Equipe.query.all()
    return render('gestion_equipes.html', page='gestion - equipes', onglet='gestion_rh', equipes=equipes)


# l'api sera décallée ici temporairement
@server.route('/api/equipe', methods=['POST'])
@Auth.role_in('rrh')
def ajouter_equipe():
    current_user = get_current_user()
    nom_equipe = request.form.get('nom_equipe')

    try:
        current_user.creer_equipe(nom_equipe)
        notify_success("Equipe créée!")
    except NomEquipeExistant:
        notify_error("Une équipe porte déjà ce nom.")

    return redirect(URL.PAGE.GESTION_EQUIPES)


@Auth.role_in('rrh')
@server.route(URL.PAGE.MODIFIER_EQUIPE)

def ctrl_modifier_equipe(id_equipe=None):
    equipe = Equipe.query.filter_by(id=id_equipe).first()
    return render(
        'modifier_equipe.html',
        equipe=equipe
    )

@Auth.role_in('rrh')
@server.route(URL.RES.MODIFIER_EQUIPE, methods=['POST'])
def ressource_modifier_equipe(id_equipe):
    current_user = get_current_user()

    nom_equipe = request.form.get('nom_equipe')

    try:
        current_user.modifier_equipe(id_equipe, nom_equipe)
        notify_success("Equipe modifiée!")
    except NomEquipeExistant:
        notify_error("Une équipe porte déjà ce nom.")
    
    return redirect(URL.PAGE.GESTION_EQUIPES)


@Auth.role_in('rrh')
@server.route('/api/equipes/delete/<int:id_equipe>', methods=['GET'])
def supprimer_equipe(id_equipe=None):
    current_user = get_current_user()

    current_user.supprimer_equipe(id_equipe)
    notify_success("Equipe supprimee")

    return redirect(URL.PAGE.GESTION_EQUIPES)
