
from app.server import server, database
from app.ctrl.utils import Auth, request, render, session, redirect, notify_error
from app.ctrl.urls import URL

# controller de la page de connexion
@server.route(URL.PAGE.CONNEXION, methods=['GET'])
def page_connexion():
    return render('connexion.html', page='connexion', navbar=False)

# ressource de deconnexion
@server.route(URL.RES.CONNEXION, methods=['POST'])
def connexion():
    login = request.form.get('login')
    passwd = request.form.get('password')
    
    user = Auth.connexion(login, passwd)

    if user:
        # recupere les attributs d'un utilisateur
        session_user = user.__dict__
        # enleve les attributs à cacher ou inutiles
        del session_user['_sa_instance_state']
        del session_user['password']

        session['user'] = session_user
        return redirect(URL.PAGE.HOME)
    else:
        notify_error('Mauvais couple (login, mot de passe)')
        return redirect(URL.PAGE.CONNEXION)


# pas de page de deconnexion

# ressource de deconnexion
@server.route(URL.RES.DECONNEXION, methods=['GET'])
@Auth.user_connected
def deconnexion():
    id_utilisateur = session['user'].get('id')
    session['user'] = None
    return redirect(URL.PAGE.CONNEXION)
