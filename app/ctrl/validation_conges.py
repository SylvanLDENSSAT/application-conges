from app.server import server
from app.ctrl.urls import URL
from app.ctrl.utils import Auth, request, render, abort, redirect, get_current_user

from app.models import Employe, Conge, Equipe
from app.models.conge import STATUT_CONGE

@server.route(URL.PAGE.VALIDATION_DEMANDES)
@Auth.role_in('rh', 'rrh')
def ctrl_validation_demandes():
    nom = request.args.get('nom')
    prenom = request.args.get('prenom')
    equipe = request.args.get('equipe')

    filters = (
        Employe.nom.ilike('%' + nom + '%'),
        Employe.prenom.ilike('%' + prenom + '%'),
        Equipe.nom.ilike('%' + equipe + '%')
    ) if request.args.get('nom')!=None else ()

    demandes = Conge.query.join(Employe).join(Equipe).filter(
        Conge.statut!=STATUT_CONGE.REFUSE,
        Conge.statut!=STATUT_CONGE.VALIDE,
        *filters
    ).all()

    return render(
        'validation_conges.html', 
        page='validation des congés', 
        conges=demandes, 
        STATUT_CONGE=STATUT_CONGE
    )


@server.route(URL.RES.VALIDATION_CONGE, methods=['POST'])
def validation_conge():
    id_conge = request.form.get('id_demande')
    action = request.form.get('action')

    current_user = get_current_user()
    conge = Conge.query.filter_by(id=id_conge).first()

    conge.commentaire = request.form.get('commentaire')

    if action == "accepter":
        current_user.valider_conge(conge)
    elif action == "refuser":
        current_user.refuser_conge(conge)
    
    return redirect(URL.PAGE.VALIDATION_DEMANDES)
