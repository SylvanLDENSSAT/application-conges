from app.server import server
from app.ctrl.utils import Auth, get_current_user, render
from app.ctrl.urls import URL

from app.models import Employe # sale, recuperer directement depuis l'equipe svp
from app.models.conge import STATUT_CONGE


# pages réservées aux managers
@Auth.user_connected # checker qu'il est bien manager au cas ou c'est un petit malin
@server.route(URL.PAGE.CONGES_EQUIPE)
def ctrl_equipe():
    current_user = get_current_user()
    employes_equipe = Employe.query.filter_by(id_equipe=current_user.equipe.id).all()
    return render('equipe.html', page='congés de l\'équipe', STATUT_CONGE=STATUT_CONGE, employes_equipe=employes_equipe)
