from app.server import server
from app.ctrl.utils import Auth, session, get_current_user, render, URL, request, abort, redirect, notify_success, notify_error
from app.models.conge import Conge, MOTIF_CONGE, STATUT_CONGE, CongesInsuffisants
from app.models.roles import Employe

# conges de l'utilisateur connecte
@server.route(URL.PAGE.MES_CONGES, methods=['GET'])
@Auth.user_connected
def ctrl_mes_demandes_get():
    conges = Conge.query.filter_by(id_employe=session['user']['id']).order_by(Conge.date_debut).all()

    return render(
        'conges.html', 
        page='mes congés', 
        conges=conges,
        motifs=[
            MOTIF_CONGE.RTT,
            MOTIF_CONGE.ANNUEL,
            MOTIF_CONGE.ENFANT_MALADE,
            MOTIF_CONGE.DECES_PROCHE
        ],
        STATUT_CONGE=STATUT_CONGE
    )


@server.route(URL.RES.SUPPRIMER_CONGE, methods=['GET'])
def supprimer_conge(id_conge=None):
    current_user = get_current_user()

    current_user.supprimer_conge(id_conge)
    notify_success("La demande a bien été supprimée!")

    return redirect(URL.PAGE.MES_CONGES)


@server.route(URL.PAGE.MODIFIER_CONGE, methods=['GET'])
def page_modifier_conge(id_conge):
    if id_conge:
        conge = Conge.query.filter_by(id=id_conge).first()
        if conge:
            return render(
                "modifier_conge.html", 
                conge=conge, 
                motifs=[
                    MOTIF_CONGE.RTT,
                    MOTIF_CONGE.ANNUEL,
                    MOTIF_CONGE.ENFANT_MALADE,
                    MOTIF_CONGE.DECES_PROCHE
                ]
            )
        else:
                return abort(404)
    else:
        return abort(400)


@server.route(URL.RES.MODIFIER_CONGE, methods=['POST'])
def modifier_conge(id_conge=None):
    current_user = get_current_user()

    date_debut = request.form.get('date_debut')
    date_fin = request.form.get('date_fin')
    motif = request.form.get('motif')

    try:
        current_user.modifier_conge(id_conge, date_debut, date_fin, motif)
        notify_success("La demande de congés a bien été modifiée.")
    except:
        notify_error("Conges insuffisants.")

    return redirect(URL.PAGE.MES_CONGES)


@server.route(URL.RES.DEMANDER_CONGE, methods=['POST'])
@Auth.user_connected
def demander_conge():
    # recupere les donnees de l'employe
    id_employe = request.form.get('id_employe')

    # donnees concernant la demande de conge
    date_debut =  request.form.get('date_debut')
    date_fin =  request.form.get('date_fin')
    motif =  request.form.get('motif')

    # recupere l'employe concerné
    employe = Employe.query.filter_by(id=id_employe).first()

    # vérifie que les paramètres passés sont corrects (même si l'interface l'impose)
    if id_employe and date_debut and date_fin and motif:
        try:
            employe.demander_conge(date_debut, date_fin, motif)
            notify_success("Demande de congés déposée!")
        except CongesInsuffisants:
            notify_error("Jours de congés restants insuffisants")
    else:
        return abort(400)

    return redirect('/conges/mes_demandes')
