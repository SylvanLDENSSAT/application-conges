class srv_conf:
    host = '0.0.0.0'
    port = 5000
    debug = True


class app_conf:
    SESSION_TYPE = 'redis'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'


class app_env:
    name = 'Congés'
    description = 'une app de geestion des congés'
