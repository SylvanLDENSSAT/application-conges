from .equipe import Equipe
from .conge import Conge

from app.models.roles import Utilisateur, Employe, RH, RRH

from app.server import database

# crée les tables associées aux modèles déclarées ci-dessus
database.create_all()

# jeux de tests equipes
enssat = Equipe(nom='ENSSAT')
orange = Equipe(nom='Orange')

# défini des jeux de test
sylvan = Employe(
    login='sledeunf',
    password='enssat',
    nom='LE DEUNFF',
    prenom='Sylvan', 
    adresse='10 rue de Kerampont',
    equipe=orange,
    mail='sledeunf@gmail.com',
    nb_conges=1
)
database.session.add(sylvan)

romain = Employe(
    login='rchantrel',
    password='enssat',
    nom='CHANTREL',
    prenom='Romain', 
    adresse='9 rue du chenes germain', 
    equipe=orange,
    mail='rchantrel@gmail.com',
    nb_conges=3
)
database.session.add(romain)

lionel = RH(
    login='lsermanson',
    password='enssat',
    nom='SERMANSON',
    prenom='Lionel', 
    adresse='Lannion', 
    equipe=enssat,
    mail='lsermanson@gmail.com',
    nb_conges=0
)
database.session.add(lionel)

abdel = Employe(
    login='arachedi',
    password='orange',
    nom='RACHEDI',
    prenom='Abdel', 
    chef_equipe=True,
    adresse='9 rue du chenes germain', 
    equipe=orange,
    mail='arachedi@orange.com',
    nb_conges=2
)
database.session.add(abdel)

baptiste = RRH(
    login='bprieur',
    password='enssat',
    nom='PRIEUR',
    prenom='Baptiste',
    adresse='10 rue de Kerampont', 
    equipe=enssat,
    mail='bprieur@orange.com',
    nb_conges=5
)
database.session.add(baptiste)

from datetime import datetime
# fonction vite fait pour instancier un objet date depuis une chaine
date = lambda string: datetime.strptime(string, '%d/%m/%Y')

conge1 = Conge(
    # recupere l'objet Employe en cascade
    employe = sylvan,
    motif = 'RTT',
    date_debut = date('22/11/2018'),
    date_fin = date('26/12/2018'),
    statut = "en attente de validation..."
)
database.session.add(conge1)

conge2 = Conge(
    # recupere l'objet Employe en cascade
    employe = sylvan,
    motif = 'RTT',
    date_debut = date('24/11/2018'),
    date_fin = date('25/12/2018'),
    statut = "en attente de validation..."
)
database.session.add(conge2)


database.session.commit()
