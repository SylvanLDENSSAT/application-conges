from app.server import database
from sqlalchemy import Column, String, Integer, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from app.models.equipe import Equipe
from app.models.conge import Conge, MOTIF_CONGE, STATUT_CONGE, CongesInsuffisants
from app.models.roles.utilisateur import Utilisateur

from datetime import datetime

class Employe(Utilisateur):
    # déclaration d'un nom de table à associer au modèle
    __tablename__ = 'employe'

    # cle etrangere qui reference l'id d'un utilisateur
    id = Column(Integer, ForeignKey('utilisateur.id'), primary_key=True)

    # attributs propres à un utilisateur
    nom = Column(String)
    prenom = Column(String)
    adresse = Column(String)
    
    mail = Column(String, unique=True)
    fonction = Column(String)

    nb_conges = Column(Integer)
    conges = relationship("Conge", back_populates="employe")

    id_equipe = Column(Integer, ForeignKey(Equipe.id), nullable=False) # id de l'equipe à laquelle appartient l'employe
    equipe = relationship('Equipe')
    chef_equipe = Column(Boolean, default=False) # indique si l'employe est le chef de son equipe

    # necessaire à l'heritage dans sqlalchemy
    __mapper_args__ = {
        'polymorphic_identity':'employe',
    }


    def demander_conge(self, date_debut, date_fin, motif):
        if motif in (MOTIF_CONGE.DECES_PROCHE, MOTIF_CONGE.ENFANT_MALADE):
            statut = STATUT_CONGE.VALIDE
        else:
            statut = STATUT_CONGE.EN_ATTENTE

        debut = datetime.strptime(date_debut, '%d/%m/%Y')
        fin = datetime.strptime(date_fin, '%d/%m/%Y')

        delta = (fin-debut).days

        if self.nb_conges - delta >= 0:
            # crée un congés avec le status "en attente de validation"
            nouvelle_demande = Conge(
                employe=self, 
                date_debut=debut, 
                date_fin=fin, 
                motif=motif,
                statut=statut
            )

            self.nb_conges -= delta 

            database.session.add(nouvelle_demande)
            database.session.commit()
        else:
            raise CongesInsuffisants


    def modifier_conge(self, id_conge, date_debut, date_fin, motif):
        # recupere le conge existant
        conge = [ conge for conge in self.conges if conge.id == id_conge ][0]

        # actualise automatiquement le statut pour les motifs spécifiques (enfant malade ...)
        if motif in (MOTIF_CONGE.DECES_PROCHE, MOTIF_CONGE.ENFANT_MALADE):
            statut = STATUT_CONGE.VALIDE
        else:
            statut = STATUT_CONGE.EN_ATTENTE

        # converti les chaines recuperees en objets de type date
        debut = datetime.strptime(date_debut, '%d/%m/%Y')
        fin = datetime.strptime(date_fin, '%d/%m/%Y')

        # duree de la demande (en jours)
        delta1 = (fin - debut).days

        # duree de la demande precedente
        delta2 = (conge.date_fin - conge.date_debut).days

        # verifie que le nombre de jours restants est suffisant
        if self.nb_conges - delta1 + delta2 >= 0:
            # modifie la demande existante
            conge.date_debut = debut
            conge.date_fin = fin
            conge.motif = motif

            # recalcule les conges restants
            self.nb_conges += delta2 - delta1

            # applique les modification en BDD
            database.session.commit()
        else:
            raise CongesInsuffisants


    def supprimer_conge(self, id_conge):
        # recupere le conge existant
        conge = [ conge for conge in self.conges if conge.id == id_conge ][0]
        # le supprime de la bdd
        database.session.delete(conge)
        # applique les changements
        database.session.commit()