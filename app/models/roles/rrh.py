from app.server import database
from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.exc import IntegrityError

from .rh import RH

class NomEquipeExistant(Exception):
    pass

class AdresseMailExistante(Exception):
    pass


class RRH(RH):
    __tablename__ = 'rrh'
    id = Column(Integer, ForeignKey('rh.id'), primary_key=True)

    # la fonction d'un RRH est RRH
    fonction = 'RRH'
    chef_equipe = True

    __mapper_args__ = {
        'polymorphic_identity':'rrh',
    }


    def creer_employe(self, role, nom, prenom, adresse, mail, id_equipe):
        from ..roles import ROLES

        TypeEmploye = ROLES.get(role, ROLES['employe'])['classe'] # Employe est le role par defaut

        try:
            nouvel_employe = TypeEmploye(
                login=mail.lower(),
                password='enssat', # piste d'amelioration
                nom=nom.upper(),
                prenom=prenom.title(),
                adresse=adresse.lower(),
                mail=mail.lower(),
                id_equipe=id_equipe,
                nb_conges=0
            )
            database.session.add(nouvel_employe)
            database.session.commit()
            return nouvel_employe
        except IntegrityError:
            database.session.rollback()
            raise AdresseMailExistante


    def supprimer_employe(self, id_employe):
        from .employe import Employe

        employe = Employe.query.filter_by(id=id_employe).first()

        for conge in employe.conges:
            database.session.delete(conge)
            database.session.commit()

        database.session.delete(employe)
        database.session.commit()


    def modifier_employe(self, id_employe, nom, prenom, adresse, mail, id_equipe):
        from .employe import Employe

        employe = Employe.query.filter_by(id=id_employe).first()

        try:
            employe.login = mail.lower()
            employe.password = 'enssat'
            employe.nom = nom.upper()
            employe.prenom = prenom.title()
            employe.adresse = adresse.lower()
            employe.mail = mail.lower()
            employe.id_equipe = id_equipe
         
            database.session.commit()
            return employe
        except IntegrityError:
            database.session.rollback()
            raise AdresseMailExistante


    def creer_equipe(self, nom_equipe):
        from ..equipe import Equipe

        try:
            nouvelle_equipe = Equipe(nom=nom_equipe)
            database.session.add(nouvelle_equipe)
            database.session.commit()
            return nouvelle_equipe
        except IntegrityError:
            database.session.rollback()
            raise NomEquipeExistant


    def modifier_equipe(self, id_equipe, nom_equipe):
        from ..equipe import Equipe

        try:
            equipe = Equipe.query.filter_by(id=id_equipe).first()
            equipe.nom = nom_equipe
            database.session.commit()
        except IntegrityError:
            database.session.rollback()
            raise NomEquipeExistant


    def supprimer_equipe(self, id_equipe):
        from ..equipe import Equipe

        equipe = Equipe.query.filter_by(id=id_equipe).first()
        database.session.delete(equipe)
        database.session.commit()
