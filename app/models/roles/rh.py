from app.server import database
from sqlalchemy import Column, String, Integer, ForeignKey

from .employe import Employe
from ..conge import STATUT_CONGE


class RH(Employe):
    __tablename__ = 'rh'
    id = Column(Integer, ForeignKey('employe.id'), primary_key=True)

    fonction = 'RH'
    chef_equipe = False

    __mapper_args__ = {
        'polymorphic_identity':'rh',
    }
    
    def valider_conge(self, conge):
        conge.statut = STATUT_CONGE.VALIDE
        database.session.commit()

    def refuser_conge(self, conge):
        conge.statut = STATUT_CONGE.REFUSE
        database.session.commit()
