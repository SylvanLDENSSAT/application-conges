#!/usr/bin/python3

from app.config import srv_conf
from app.server import server

if __name__ == '__main__':
    # add routes to server object
    import app.ctrl

    # decommentez pour ouvrir dans un navigateur au lancement du serveur
    # import webbrowser
    # webbrowser.open("http://localhost:5000")

    # run the web server
    server.run(host=srv_conf.host, port=srv_conf.port, debug=True)
