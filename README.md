# Projet UML - Gestion des congés

> Application servant à gérer les congés, aussi bien pour les employés que les RH.

## Pré-requis
* le langage python3 ```//attention à ne pas utiliser python2```

Vous trouverez à la racine du projet un fichier *requirements.txt* qui contient la liste des modules à installer pour satisfaire les dépendances du projet.

Pour les installer, il suffit d'utiliser **pip** (le gestionnaire de packages python) comme indiqué ci-dessous :
```
pip install -r requirements.txt
```


## Description du livrable

Ce projet se présente sous la forme d'une application WEB packagée avec un serveur.

Pour être utilisé, vous pouvez soit



* l'ouvrir manuellement dans un navigateur en ouvrant l'url 
```
http://localhost:5000
```
* le lancer dans un navigateur en même temps que le serveur, pour celà décommentez dans le fichier ```run``` les lignes:
```
# import webbrowser
# webbrowser.open('http://localhost:5000')
```



## Lancer le serveur
Lancez la commande
```
python run.py
```